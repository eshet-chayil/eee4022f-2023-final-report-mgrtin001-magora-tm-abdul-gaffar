%% Multiple Scatterer Autofocus

function ISAR_image = MultipleScattererAutofocus(hywrd_aligned_profiles)

% Define all constants and variables
criteria = 0.16;
NumberOfScatterers = 11; % retain the 1st 11 scatterers that satisfy the criteria 

variance = var(abs(hywrd_aligned_profiles),1);       % calculate variance for each column (range bin)
mean_squared = mean(abs(hywrd_aligned_profiles)).^2; % calculate the mean squared for each column                                   
x = variance./(variance + mean_squared);
[~, RangeBins_Idx] = sort(x);

% calculate range bins of the 1st alignment range profile that meet the criteria
AscendingBins = hywrd_aligned_profiles(:, RangeBins_Idx); % matrix B(NxM)

RefBins = AscendingBins(1, 1:NumberOfScatterers); % 1st row, range bins: m = 1 to 11

phases = zeros(size(AscendingBins, 1), 1);

for row = 1:size(AscendingBins, 1) % Loop through N aligned range profiles

    % obtain N elements of the phase shift/correction vector
    Product_vector = conj( RefBins ) .* AscendingBins(row, 1:NumberOfScatterers);
    AverageVal_scalar = sum(Product_vector) / NumberOfScatterers ;
    phases(row) = angle(AverageVal_scalar);
end

% use the phase correction vector to obtain the complex phase correction
% vector (magnitude = 1)
CorrectionVector = exp(-1i * phases);

% apply the correction vector to each range bin of the aligned range profile matrix
PhaseCorrectedProfiles = hywrd_aligned_profiles .* CorrectionVector;

% apply hamming window function
window = hamming(size(hywrd_aligned_profiles,1));

% apply the FFT in the slow-time domain on each range aligned and phase
% compensated range bin to generate an ISAR image.
ISAR_image = fftshift(fft(PhaseCorrectedProfiles .* window,[],1),1);
