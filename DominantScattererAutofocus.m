%% Dominant Scatterer Autofocus

function ISAR_image = DominantScattererAutofocus(alignedHRR_profiles, ScalingFactor)

% calculate the amplitude variance of each range bin, m = 1...n
AmplitudeVariances = var(abs(alignedHRR_profiles),1);

% 2nd criteria to determine the dominant scatterer
D_Scat_Compare = sum(abs(alignedHRR_profiles).^2, 1);
D_Scat_Criteria = mean(D_Scat_Compare);

%figure; plot(D_Scat_Compare); hold on; plot(D_Scat_Criteria*ScalingFactor*ones(1, length(D_Scat_Compare)))

% Find all range bins that satisfy criteria 2
RangeBins_Idx = find(D_Scat_Compare > ScalingFactor*D_Scat_Criteria);

% 1st criteria to determine the dominant scatterer
% calculate the range bin with the minimum amplitude variance
[~, minIndex]  = min(AmplitudeVariances(RangeBins_Idx)); 

RangeBinFound = RangeBins_Idx(minIndex); 

% figure; imagesc(20*log10(abs(alignedHRR_profiles))); 
% colorbar; xlabel('Range(bins)'); ylabel('Profile number')                             
% title('Aligned profiles'); colormap('jet')                                               

% obtain the complex conjugate of dominant scatterer across all range 
% profiles and let this be called a "correction vector"
aligned_phases = angle(alignedHRR_profiles(:,RangeBinFound));
CorrectionVector = exp(-1i * aligned_phases);

% apply the correction vector to each range bin of the aligned range profile matrix
PhaseCorrectedProfiles = alignedHRR_profiles .* CorrectionVector;

% apply hamming window function
window = hamming(size(alignedHRR_profiles,1));

% apply the FFT in the slow-time domain on each range aligned and phase
% compensated range bin to generate an ISAR image.
ISAR_image = fftshift(fft(PhaseCorrectedProfiles .* window,[],1),1);                                