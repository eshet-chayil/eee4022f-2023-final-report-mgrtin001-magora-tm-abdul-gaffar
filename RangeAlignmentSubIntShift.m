%% Hayward Range Alignment

function AlignedProfiles = RangeAlignmentSubIntShift(unalignedRangeProfiles, Range_axis, NumProfiles_axis)

NumProfiles = size(unalignedRangeProfiles,1);
NumRangeBins = size(unalignedRangeProfiles,2);

% Obtain the magnitude of the reference profile                                                     
RefProfile = abs(unalignedRangeProfiles(1,:));                                                      

% Perform the cross-correlation
corr_matrix = conv2(fliplr(abs(unalignedRangeProfiles)),RefProfile);                                % conv2 is faster than xcorr in MATLAB. 

% Locate the peak and the required bin shift
[~,Index] = max(corr_matrix.');                                                                  % Computing max value and the index 
binshifts = Index - NumRangeBins ;                                                                  % Calculate the bin shifts required 

% Obtain non-integer bin-shifts to align profiles 
NumProfilesVector = (0:1:(NumProfiles-1));
order = 3; 
calc_coeff = polyfit(NumProfilesVector,binshifts,order);                    % Fitting the curve using a low order polynomial 
binshifts_non_integer = polyval(calc_coeff,NumProfilesVector);              % getting non integer bin shifts

% Shift profiles by non-integer values by taking data to the frequency domain and multiplying with a phase ramp 
vector_idx = (-NumRangeBins/2:1:(NumRangeBins/2-1));
Complex_PhaseRamp = exp(-1i*2*pi*(binshifts_non_integer.').*vector_idx/NumRangeBins);               
AlignedProfiles = ifft(Complex_PhaseRamp.*(fft(unalignedRangeProfiles,[],2)),[],2);                       

% Plot the integer bin shifts needed
% figure;                                                                                             
% plot(binshifts)                                                                                      
% hold on;                                                                                            
% plot(binshifts_non_integer);                                                                        
% grid on                                                                                             
% xlabel("Profile Number")                                                                            
% ylabel("Shifts required")                                                                           
% title('Number of shifts required per profile')                                                      
% legend("Integer bin shifts", "Non-integer bin shifts");                                              

% Plot unaligned profiles
% figure;
% subplot(1,2,1)
% imagesc(Range_axis, NumProfiles_axis, 20*log10(abs(unalignedRangeProfiles)));
% colorbar; xlabel('Range (m)'); ylabel('Profile number');
% %title('Unaligned - subset 10');
% axis xy; colormap('jet');

% Plot aligned profiles
figure;
%subplot(1,2,2)
imagesc(Range_axis, NumProfiles_axis, 20*log10(abs(AlignedProfiles)));
colorbar; xlabel('Range (m)'); ylabel('Profile number');
%title('Aligned - subset 10');
colormap('jet'); axis xy;