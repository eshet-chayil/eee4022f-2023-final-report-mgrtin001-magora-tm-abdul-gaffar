close all; 
clear all;
%clc;

%% Load CSIR dataset 

load('DAP_2010-10-14_09-43-33_010_zayaan_inbound_singlebin_P455_G1_sb_HRR.mat');
HRRProfilesAll = sb_HRR.G1.HRR_NoMC_calib.';
ProfileRepetitionFreq =  1/sb_HRR.G1.pattern_time; 
NumRangeBins = size(HRRProfilesAll,1);
NumOfProfiles = size(HRRProfilesAll,2);
Range_axis = sb_HRR.G1.xaxis_downrange_m; 
ScalingFactor = 1;

% % Plot the HRR profiles
% figure; imagesc(sb_HRR.G1.xaxis_downrange_m, 1:size(HRRProfilesAll,1), 20*log10(abs(HRRProfilesAll)));
% xlabel('Range (m)');
% ylabel('Profile Number');
% title('All HRR profiles');
% colormap('jet');
% colorbar;

% Select a subset of profiles
MiddleProfile = 2464;                                              % 2464, 4189, 2970, 3827
CPTWL = 128;   
StartProfile = MiddleProfile - CPTWL/2;                           
StopProfile = MiddleProfile + CPTWL/2 - 1;

ProfilesToProcess = StopProfile - StartProfile + 1;
DopplerAxis_Hz = (-ProfilesToProcess/2:1:ProfilesToProcess/2-1)*ProfileRepetitionFreq/ProfilesToProcess;

% Plot Unaligned HRR Profiles

HRR_profiles = circshift(HRRProfilesAll(StartProfile:StopProfile, :), [0 50 ]);
NumProfiles_axis = 1:size(HRR_profiles,1);
figure; imagesc(sb_HRR.G1.xaxis_downrange_m, NumProfiles_axis, 20*log10(abs(HRR_profiles)) ); 
xlabel('Range (m)');
ylabel('Profile Number');
title('Subset of HRR profiles');
colormap('jet');
colorbar;

% Plot Aligned HRR profiles
hywrd_aligned_profiles = RangeAlignmentSubIntShift(HRR_profiles, Range_axis, NumProfiles_axis);

%% Plot unfocused ISAR image - no range alignment, no autofocus

% apply hamming window function
window = hamming(size(ProfilesToProcess,1));

ISAR_image = fftshift(fft(HRR_profiles.*window,[],1),1); % Apply FFT in the slow-time dimension                    

ISAR_image_dB = Normalise_limitDynamicRange_ISAR_dB(ISAR_image, 35);    % Limit dynamic range of ISAR image 

% Plot unfocused ISAR image
figure; imagesc( Range_axis, DopplerAxis_Hz, ISAR_image_dB );                                
colorbar; xlabel('Range(m)'); axis ij; ylabel('Doppler frequency (Hz)')                                           
title('Unfocused ISAR Image - no TMC (subset 12)'); colormap('jet')
% axis xy;

IC = calculateImageContrast(ISAR_image);
fprintf('Unfocused ISAR Image - no RA, no AF: Image contrast = %s (StartProfile = %d - StopProfile = %f) \n', IC, StartProfile, StopProfile)

%% Plot focused ISAR image - with range alignment and dominant scatterer autofocus

ISAR_image = DominantScattererAutofocus(hywrd_aligned_profiles, ScalingFactor);
ISAR_image_dB = Normalise_limitDynamicRange_ISAR_dB(ISAR_image, 35);    % Limit dynamic range of ISAR image 

figure; imagesc( Range_axis, DopplerAxis_Hz, ISAR_image_dB );                                   
colorbar; xlabel('Range(m)'); ylabel('Doppler frequency (Hz)')                                               
caption = sprintf('ISAR Image - DSA (subset 12)'); % StartProfile, StopProfile)
title(caption); colormap('jet')
% axis xy;

IC = calculateImageContrast(ISAR_image);
fprintf('Focused ISAR Image - DSA: Image contrast = %s (StartProfile = %d - StopProfile = %f) \n', IC, StartProfile, StopProfile)

%% Plot focused ISAR image - with range alignment and multiple scatterer autofocus

ISAR_image = MultipleScattererAutofocus(hywrd_aligned_profiles);
ISAR_image_dB = Normalise_limitDynamicRange_ISAR_dB(ISAR_image, 35);    % Limit dynamic range of ISAR image 

figure; imagesc( Range_axis, DopplerAxis_Hz, ISAR_image_dB);                                   
colorbar; xlabel('Range(m)'); ylabel('Doppler frequency (Hz)')                                               
caption2 = sprintf('ISAR Image - MSA (subset 12)'); %, StartProfile, StopProfile)
title(caption2); colormap('jet')
%axis xy;

IC = calculateImageContrast(ISAR_image);

fprintf('Focused ISAR Image - MSA: Image contrast = %s (StartProfile = %d - StopProfile = %f) \n', IC, StartProfile, StopProfile)

%%
%rectangle('Position',[0 2432 29.6578 63],'EdgeColor',"k","LineWidth",1);
%rectangle('Position',[0 2400 29.6578 127],'EdgeColor',"k","LineWidth",1)
% rectangle('Position',[0 2336 29.6578 255],'EdgeColor',"k","LineWidth",1)

%rectangle('Position',[0 2938 29.6578 63],'EdgeColor',"k","LineWidth",1);
%rectangle('Position',[0 2906 29.6578 127],'EdgeColor',"k","LineWidth",1)
% rectangle('Position',[0 2842 29.6578 255],'EdgeColor',"k","LineWidth",1)

%rectangle('Position',[0 3795 29.6578 63],'EdgeColor',"k","LineWidth",1);
%rectangle('Position',[0 3763 29.6578 127],'EdgeColor',"k","LineWidth",1)
% rectangle('Position',[0 3699 29.6578 255],'EdgeColor',"k","LineWidth",1)

%rectangle('Position',[0 4157 29.6578 63],'EdgeColor',"k","LineWidth",1);
%rectangle('Position',[0 4125 29.6578 127],'EdgeColor',"k","LineWidth",1)
% rectangle('Position',[0 4061 29.6578 255],'EdgeColor',"k","LineWidth",1)

%%

% calculate the amplitude variance of each range bin, m = 1...n
%AmplitudeVariances = var(abs(hywrd_aligned_profiles),1);

% 2nd criteria to determine the dominant scatterer
% D_Scat_Compare = sum(abs(hywrd_aligned_profiles).^2, 1);
% D_Scat_Criteria = mean(D_Scat_Compare);

%figure; plot(D_Scat_Compare); hold on; plot(D_Scat_Criteria*ScalingFactor*ones(1, length(D_Scat_Compare)))

% Find all range bins that satisfy criteria 2
%RangeBins_Idx = find(D_Scat_Compare > ScalingFactor*D_Scat_Criteria);

% 1st criteria to determine the dominant scatterer
% calculate the range bin with the minimum amplitude variance
%[~, minIndex]  = min(AmplitudeVariances(RangeBins_Idx)); 

%RangeBinFound = RangeBins_Idx(minIndex); 

% figure; imagesc(20*log10(abs(alignedHRR_profiles))); 
% colorbar; xlabel('Range(bins)'); ylabel('Profile number')                             
% title('Aligned profiles'); colormap('jet')                                               

% obtain the complex conjugate of dominant scatterer across all range 
% profiles and let this be called a "correction vector"
% aligned_phases = angle(hywrd_aligned_profiles(:,RangeBinFound));
% aligned = unwrap(angle(hywrd_aligned_profiles(:,30)));

% figure
% plot(aligned);
% xlabel('Profile Number');
% ylabel('Phase function (degrees)');
% xlim([0 130])
% title("Before")

% CorrectionVector = exp(-1i * aligned_phases);
% ACV = unwrap(angle(CorrectionVector));
% figure
% plot(ACV)
% title("ACV")
% % apply the correction vector to each range bin of the aligned range profile matrix
% phaseCorrectedHRR_Profiles = hywrd_aligned_profiles .* CorrectionVector;
% aligned_correct = unwrap(angle(phaseCorrectedHRR_Profiles(:,30)));

% figure
% plot(aligned_correct);
% xlabel('Profile Number');
% ylabel('Phase function (radians)');
% title("After")
% xlim([0 130]);