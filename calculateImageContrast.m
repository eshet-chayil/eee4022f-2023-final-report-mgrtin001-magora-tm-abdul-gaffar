%% Image Contrast

function ImageContrast = calculateImageContrast(ISAR_image)

IsarImageIntensity = abs(ISAR_image).^2;

mean_IsarImageIntensity= mean(IsarImageIntensity,'all');

numerator = sqrt(mean((IsarImageIntensity - mean_IsarImageIntensity).^2,'all'));

ImageContrast = numerator/mean_IsarImageIntensity;

end 