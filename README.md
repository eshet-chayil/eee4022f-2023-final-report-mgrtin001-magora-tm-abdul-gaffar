# EEE4022F-2023-FINAL-REPORT-MGRTIN001-MAGORA-TM---ABDUL-GAFFAR

## Comparing the performance of autofocus algorithms in Inverse Synthetic Aperture Radar (ISAR) imaging of sea vessels.

## Project Description
Radar images of boats contain rich information for classification purposes. This has applications for civilian wide-area persistent surveillance of sea in the Economic Exclusion Zone. A 2-D radar image of a boat is the output of an Inverse Synthetic Aperture Radar (ISAR) technique that is focused on removing the translational motion of an object. Translation motion compensation is made up of two steps: range alignment and autofocus.

In this project, the dominant scatterer autofocus (DSA) and multiple scatterer autofocus (MSA) algorithms are explored to identify which algorithm leads to superior performance. The criteria for best performance. The criteria for best performance is the formation of highly focused ISAR images and computational efficiency.

## Research Objectives
This research seeks to achieve the following objectives:

1. Implement the sub-integer range bin alignment technique.
2. Implement the DSA algorithm and apply to measured data of sea vessel(s).
3. Implement the MSA algorithm and apply to measured data of sea vessel(s)
4. Compare the performance of the DSA and the MSA algorithms by qualitatively
observing the final ISAR image(s) and quantitatively computing the image contrast
of the final image(s)

## Usage
- Step 1: Download the zipped folder that contains the six scripts [LoadRadarCSIRData.m](./LoadRadarCSIRData.m), [RangeAlignmentSubIntShift.m](./RangeAlignmentSubIntShift.m), [DominantScattererAutofocus.m](./DominantScattererAutofocus.m), [MultipleScattererAutofocus.m](./MultipleScattererAutofocus.m), [Normalise_limitDynamicRange_ISAR_dB.m](./Normalise_limitDynamicRange_ISAR_dB.m) and [calculateImageContrast.m](calculateImageContrast.m). [LoadRadarCSIRData.m](./LoadRadarCSIRData.m) is the main program and needs all other scripts to be in the same folder in order to run.

- Step 2: Find the following line of code in [LoadRadarCSIRData.m](./LoadRadarCSIRData.m) (line 25) CPTWL = 128; Change the number of range profiles to be processed as desired (selecting range profiles to be worked on by the range alignment algorithm and autofocus algorithms).

- Step 3: Find the following line of code in [LoadRadarCSIRData.m](./LoadRadarCSIRData.m) (line 24) MiddleProfile = 2464; Change the middle range profile number as desired. This is the middle range profile of the range profiles to be processed in Step 2.

- Step 4: Run the code. Once the code has completed running, graphs are going to be displayed. The graphs have titles to easily identify which autofocus algorithm resulted in which graph. Additionally, the image contrast value is going to be displayed in the Command Window in linear units.

**Note:** The CSIR data used for this work is not included in this public repository. In the case that the downloaded zipped folder does not work. Download the individual codes and put them in the same folder and run the main program.

## License
This repository is licensed under the [MIT LICENSE](LICENSE)

